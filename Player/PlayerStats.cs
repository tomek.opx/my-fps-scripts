using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerStats : MonoBehaviour
{
    [SerializeField] private int health = 100, currentHealth;

    [SerializeField] Healthbar healthbar;
    [SerializeField] GameObject hitEffect;
    [SerializeField] GameObject gameOver;

    void Start()
    {
        currentHealth = health;
        healthbar.SetMaxHealth(health);
    }

    public void TakeDamage(int dmg)
    {
        currentHealth -= dmg;

        healthbar.SetHealth(currentHealth);

        //SoundManager.PlaySound("playerHurt");

       // Instantiate(hitEffect, transform.position, hitEffect.transform.rotation);

        if (currentHealth <= 0) Die();
    }
    public void Die()
    {
        //SoundManager.PlaySound("playerDie");

        gameOver.SetActive(true);

        Invoke("ReloadScene", 2f);

        Debug.Log("player die");
    }
    void ReloadScene()
    {
        gameOver.SetActive(false);

        SceneManager.LoadScene("TEST");

    }
    public void RestoreHP()
    {
        currentHealth = health;
        healthbar.SetHealth(currentHealth);

    }
}
