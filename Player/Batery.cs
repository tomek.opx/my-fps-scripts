using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Batery : MonoBehaviour
{

    public Slider bateryBar;

    private float maxBatery = 100, currentBatery;
    public float CurrentBatery { get { return currentBatery; } set { currentBatery = value; } }

    private WaitForSeconds regenTick = new WaitForSeconds(0.8f);
    private Coroutine regen;

    public static Batery instance;

    // Start is called before the first frame update
    void Start()
    {
        currentBatery = maxBatery;
        bateryBar.maxValue = maxBatery;
        bateryBar.value = maxBatery;
    }

    // Update is called once per frame
    void Update()
    {
        if(currentBatery > 1)
        {
            if (Input.GetKey(KeyCode.LeftShift))
            {
                UseBatery(0.05f);
            }
        }
    }
    public void UseBatery(float amount)
    {
        if(currentBatery - amount >= 0)
        {
            currentBatery -= amount;
            bateryBar.value = currentBatery;

            if(regen != null)
            {
                StopCoroutine(regen);
            }
            regen = StartCoroutine(RegenBatery());
        }
    }

    private IEnumerator RegenBatery()
    {
        yield return new WaitForSeconds(1);

        while(currentBatery < maxBatery)
        {
            currentBatery += maxBatery / 100;
            bateryBar.value = currentBatery;
            yield return regenTick;
        }
    }
}
