using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Enemy2 : MonoBehaviour
{
    [SerializeField] private float timeBetweenAttacks = 3, sightRange = 20, attackRange = 5, shotForce = 150, stageTwoShotForce = 50, stage2TBA = 1.5f, walkPointRange;
     [SerializeField] private bool playerInSightRange,playerInAttackRange,alerted = false, stageTwo = false;
    [SerializeField] private int maxHealth = 400, damage = 20, stageTwoLimit = 400;
    [SerializeField] private Transform[] firePoints;
    [SerializeField] private Transform[] stageTwoFirePoints;
    [SerializeField] private GameObject bullet, stageTwoWeapon, shield;



     // Explosion effect

     [SerializeField] private float explosionRadius = 5f, explosionForce = 700;
     [SerializeField] private GameObject explosionEffect;
     [SerializeField] private int explosionDamage = 20;
     [SerializeField] private Transform explosionPoint;


     [SerializeField] private int currentHealth;

    bool walkPointSet = false, alreadyAttacked = false, attack = false ;

     [SerializeField] LayerMask whatIsGround, whatIsPlayer;


    [SerializeField] Healthbar healthbar;


    //sight
    [SerializeField] private float fieldOfViewAngle = 130f, losRadius = 45f;

    //memory
    private bool aiMemorizedPlayer = false;
    [SerializeField] private float memoryStartTime = 10f;
    private float increasingMemoryTime;

    //hearing
    Vector3 noisePosition;
    private bool aiHeardPlayer = false, canSpin = false;
    [SerializeField] private float noiseTravelDistance = 30f, spinSpeed = 3f, spinTime = 3f;
    private float isSpiningTime;


    bool playerIsInLOS = false, alarmed = false;

    //patroling
    public List<Transform> wayPointList;
    private int randomWayPoint;

    //navigation
    [SerializeField] NavMeshAgent enemy;
    [SerializeField] Transform target;



    Vector3 walkPoint;



    // Start is called before the first frame update
    void Start()
    {
        currentHealth = maxHealth;
        healthbar.SetMaxHealth(maxHealth);
        target = GameObject.FindWithTag("LookHere").transform;
        Patroling();
    }
    private void Update()
    {
        /*Vector3 targetPosition = new Vector3(target.position.x, this.transform.position.y, target.position.z);
        transform.LookAt(targetPosition);
       // transform.LookAt(target.transform);
                healthbar.SetHealth(currentHealth);

        playerInSightRange = Physics.CheckSphere(transform.position, sightRange, whatIsPlayer);
        playerInAttackRange = Physics.CheckSphere(transform.position, attackRange, whatIsPlayer);

        if (!playerInSightRange && !playerInAttackRange && !attack)
        {
            Patroling();
        }

        if (playerInSightRange && !playerInAttackRange)
        {
            transform.LookAt(target.transform);

            ChasePlayer();
        }

        if (playerInSightRange && playerInAttackRange)
        {
            if (!stageTwo)
                Attack();
            else
                StageTwoAttack();
        }*/
        healthbar.SetHealth(currentHealth);

        if (alarmed)
        {
            //transform.LookAt(target.transform);

        }
        float distance = Vector3.Distance(target.position, transform.position);
        if (distance <= losRadius)
        {
            CheckLOS();
        }
        if (!playerIsInLOS && !aiMemorizedPlayer && !aiHeardPlayer)
        {
            Patroling();
            NoiseCheck();
            StopCoroutine(AiMemory());
        }
        else if (aiHeardPlayer && !playerIsInLOS && !aiMemorizedPlayer)
        {
            canSpin = true;
            GoToNoisePosition();

        }
        else if (playerIsInLOS) ///////
        {
            if (playerIsInLOS && distance <= attackRange)
            {
                Attack();
            }
            else
            {
                aiMemorizedPlayer = true;
                transform.LookAt(target.transform);
                ChasePlayer();

            }
        }
        else if (aiMemorizedPlayer && !playerIsInLOS)
        {
            ChasePlayer();
            StartCoroutine(AiMemory());
        }


    }
    private void GoToNoisePosition()
    {
        enemy.SetDestination(noisePosition);
        if (Vector3.Distance(transform.position, noisePosition) <= 5f && canSpin)
        {
            isSpiningTime += Time.deltaTime;

            transform.Rotate(Vector3.up * spinSpeed, Space.World);
            if (isSpiningTime >= spinTime)
            {
                canSpin = false;
                aiHeardPlayer = false;
                isSpiningTime = 0f;
            }
        }
    }
    private IEnumerator AiMemory()
    {
        increasingMemoryTime = 0;
        while (increasingMemoryTime < memoryStartTime)
        {
            increasingMemoryTime += Time.deltaTime;
            aiMemorizedPlayer = true;
            yield return null;
        }
        aiHeardPlayer = false;
        aiMemorizedPlayer = false;
    }

    private void NoiseCheck()
    {
        float distance = Vector3.Distance(target.position, transform.position);
        if (distance <= noiseTravelDistance)
        {
            if (Input.GetButton("Fire1"))
            {
                noisePosition = target.position;

                aiHeardPlayer = true;
            }
            else
            {
                aiHeardPlayer = false;
                canSpin = false;
            }
        }
    }
    void CheckLOS()
    {
        Vector3 direction = target.position - transform.position;

        float angle = Vector3.Angle(direction, transform.forward);

        if (angle < fieldOfViewAngle * 0.5f)
        {
            RaycastHit hit;
            if (Physics.Raycast(transform.position, direction.normalized, out hit, losRadius))
            {
                if (hit.collider.tag == "Player" || hit.collider.tag == "Shootable")
                {
                    playerIsInLOS = true;
                    Debug.Log("player i LOS");
                }
                else
                {
                    playerIsInLOS = false;
                }
            }
        }
    }



    public void TakeDamage(int dmg)
    {
        currentHealth -= dmg;

        if (currentHealth <= stageTwoLimit)
        {
            stageTwo = true;
            shield.SetActive(false);
            // add some efekt!
        }

        if (currentHealth <= 0)
        {
            Die();
        }
    }
    private void Die()
    {

        //Explosion();

        Destroy(gameObject, 2f) ;
    }
    private void Explosion()
    {
        GameObject explosionGO = Instantiate(explosionEffect, explosionPoint.position, transform.rotation);

        Collider[] colliders = Physics.OverlapSphere(transform.position, explosionRadius);

        foreach (Collider nearbyObject in colliders)
        {
            Rigidbody rb = nearbyObject.GetComponent<Rigidbody>();
            if (rb != null)
            {
                rb.AddExplosionForce(explosionForce, transform.position, explosionRadius);
            }
        }

        Destroy(explosionGO, 1f);
    }
    private void Attack()
    {

        enemy.SetDestination(transform.position);

        transform.LookAt(target.transform);


        if (!alreadyAttacked)
        {
            // attack
            for (int i = 0; i< firePoints.Length; i++)
            {
                Rigidbody rb = Instantiate(bullet, firePoints[i].position, bullet.transform.rotation).GetComponent<Rigidbody>();

                rb.AddForce(firePoints[i].forward * shotForce, ForceMode.Impulse);
                rb.AddForce(firePoints[i].up * 3f, ForceMode.Impulse);

                //Destroy(rb, 1f);
                // audioSource.PlayOneShot(hitSound);
                //SoundManager.PlaySound("enemyHit");

            }

            alreadyAttacked = true;
            Patroling();
            Invoke("ResetAttack", timeBetweenAttacks);
        }
    }
    void StageTwoAttack()
    {
        enemy.SetDestination(transform.position);



        if (!alreadyAttacked)
        {
            for (int i = 0; i < firePoints.Length; i++)
            {
                Rigidbody rb = Instantiate(stageTwoWeapon, stageTwoFirePoints[i].position, stageTwoWeapon.transform.rotation).GetComponent<Rigidbody>();

                rb.AddForce(stageTwoFirePoints[i].forward * stageTwoShotForce, ForceMode.Impulse);
                rb.AddForce(stageTwoFirePoints[i].up * 3f, ForceMode.Impulse);

                //Destroy(rb, 1f);
                // audioSource.PlayOneShot(hitSound);
                //SoundManager.PlaySound("enemyHit");

            }




            alreadyAttacked = true;
            Patroling();
            Invoke("ResetAttack", stage2TBA);
        }
    }

    private void ResetAttack()
    {
        alreadyAttacked = false;
    }
    private void Patroling()
    {

        if (!walkPointSet)
        {
            SerchWalkPoint();
        }
        if (walkPointSet)
        {
            enemy.SetDestination(walkPoint);
        }

        Vector3 distanceToWalkPoint = transform.position - walkPoint;
        if (distanceToWalkPoint.magnitude < 1f) walkPointSet = false;
    }
    private void SerchWalkPoint()
    {
        float randomZ = Random.Range(-walkPointRange, walkPointRange);
        float randomX = Random.Range(-walkPointRange, walkPointRange);

        walkPoint = new Vector3(transform.position.x + randomX, transform.position.y, transform.position.z + randomZ);
        if (Physics.Raycast(walkPoint, transform.up, 2f, whatIsGround))
        {
            walkPointSet = true;

        }

    }



        private void ChasePlayer()
    {

        enemy.SetDestination(target.position);
    }
    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, attackRange);
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, sightRange);
    }
}
