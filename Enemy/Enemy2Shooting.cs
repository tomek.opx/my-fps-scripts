using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy2Shooting : MonoBehaviour
{
    [SerializeField] Transform target;
    [SerializeField] Transform[] firePoints;

    [SerializeField] private GameObject bullet;


    [SerializeField] private float shotForce = 20f, shotRate =4f, attackRange = 40;
    [SerializeField] private LayerMask whatIsPlayer;

    private bool playerInAttackRange;


    // Update is called once per frame
    private void Update()
    {
        transform.LookAt(target.transform);

        playerInAttackRange = Physics.CheckSphere(transform.position, attackRange, whatIsPlayer);

    }
    public void Shoot()
    {
        transform.LookAt(target.transform);

        for (int i = 0; i < firePoints.Length; i++)
        {
            Rigidbody rb = Instantiate(bullet, firePoints[i].position, bullet.transform.rotation).GetComponent<Rigidbody>();

            rb.AddForce(firePoints[i].forward * shotForce, ForceMode.Impulse);
            rb.AddForce(firePoints[i].up * 3f, ForceMode.Impulse);

            Destroy(rb, 1f);

        }
    }
}
