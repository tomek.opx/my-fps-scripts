using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    public GameObject enemyPrefab;
    public Transform spawnPlace;
    [SerializeField] private float spawnRate = 5f;
    private bool alreadySpawned;

    // Update is called once per frame
    void Update()
    {
        if (!alreadySpawned)
        {
            alreadySpawned = true;
            StartCoroutine("Spawn");
        }
    }
    IEnumerator Spawn()
    {
        Instantiate(enemyPrefab, spawnPlace);
        yield return new WaitForSeconds(spawnRate);
        alreadySpawned = false;
    }
}
