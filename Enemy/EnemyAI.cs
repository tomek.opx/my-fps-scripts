using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyAI : MonoBehaviour
{
    // Attack

     [SerializeField] private float timeBetweenAttacks = 3, attackRange, shotForce = 20f;
     [SerializeField] private bool playerInSightRange,playerInAttackRange,alerted = false;
     [SerializeField] private GameObject bullet;
     [SerializeField] Transform firePoint;
     [SerializeField] private GameObject hitEffect;
    [SerializeField] private ParticleSystem muzzleFlash;

    // Explosion effect

    [SerializeField] private float explosionDelay = 3f, explosionRadius = 5f, explosionForce = 700;
     [SerializeField] private GameObject explosionEffect;
     [SerializeField] private int explosionDamage = 20;
     [SerializeField] private Transform explosionPoint;

    //sight
     [SerializeField] private float sightRange, fieldOfViewAngle = 130f, losRadius = 45f;

    //memory
    private bool aiMemorizedPlayer = false;
    [SerializeField] private float memoryStartTime = 10f;
    private float increasingMemoryTime;

    //hearing
    Vector3 noisePosition;
    private bool aiHeardPlayer = false, canSpin = false;
    [SerializeField] private float noiseTravelDistance = 30f, spinSpeed = 3f, spinTime = 3f;
    private float isSpiningTime;

    //stats
    private int  currentHealth;
    [SerializeField] private int maxHealth = 50;

    bool  alreadyAttacked = false, playerIsInLOS = false, alarmed = false;

    //patroling
    public List<Transform> wayPointList;
    private int randomWayPoint;

    //navigation
     [SerializeField] NavMeshAgent enemy;
     [SerializeField] Transform target;

    //sound
    /*[SerializeField] private AudioClip hitSound, hurhSound, dieSound;
    [SerializeField] private AudioSource audioSource;*/

    // Start is called before the first frame update
    void Start()
    {
        currentHealth = maxHealth;
        target = GameObject.FindGameObjectWithTag("Player").transform;
        Patroling();
    }
    private void Update()
    {
        if (alarmed)
        {
            //transform.LookAt(target.transform);

        }
        float distance = Vector3.Distance(target.position, transform.position);
        if (distance <= losRadius)
        {
            CheckLOS();
        }
        if (!playerIsInLOS && !aiMemorizedPlayer && !aiHeardPlayer)
        {
            Patroling();
            NoiseCheck();
            StopCoroutine(AiMemory());
        }
        else if (aiHeardPlayer && !playerIsInLOS && !aiMemorizedPlayer)
        {
            canSpin = true;
            GoToNoisePosition();

        }
        else if (playerIsInLOS) ///////
        {
            if (playerIsInLOS && distance <= attackRange)
            {
                Attack();
            }
            else
            {
                aiMemorizedPlayer = true;
                transform.LookAt(target.transform);
                ChasePlayer();

            }
        }
        else if (aiMemorizedPlayer && !playerIsInLOS)
        {
            ChasePlayer();
            StartCoroutine(AiMemory());
        }
    }
        private void GoToNoisePosition()
        {
        enemy.SetDestination(noisePosition);
        if (Vector3.Distance(transform.position, noisePosition) <= 5f && canSpin)
        {
            isSpiningTime += Time.deltaTime;

            transform.Rotate(Vector3.up * spinSpeed, Space.World);
            if(isSpiningTime >= spinTime)
            {
                canSpin = false;
                aiHeardPlayer = false;
                isSpiningTime = 0f;
            }
        }
        }
    
    

    private IEnumerator AiMemory()
    {
        increasingMemoryTime = 0;
        while(increasingMemoryTime < memoryStartTime)
        {
            increasingMemoryTime += Time.deltaTime;
            aiMemorizedPlayer = true;
            yield return null;
        }
        aiHeardPlayer = false;
        aiMemorizedPlayer = false;
    }

    private void NoiseCheck()
    {
        float distance = Vector3.Distance(target.position, transform.position);
        if(distance <= noiseTravelDistance)
        {
            if (Input.GetButton("Fire1"))
            {
                noisePosition = target.position;

                aiHeardPlayer = true;
            }
            else
            {
                aiHeardPlayer = false;
                canSpin = false;
            }
        }
    }

    public void TakeDamage(int dmg)
    {
        currentHealth -= dmg;

        //SoundManager.PlaySound("enemyHurt");
        alarmed = true;

        if (currentHealth <= 0)
        {
            Die();
        }
    }
    private void Die()
    {

        // audioSource.PlayOneShot(dieSound);
        //SoundManager.PlaySound("enemyDie");

        Explosion();

        Destroy(gameObject) ;
    }
    private void Explosion()
    {
        GameObject explosionGO = Instantiate(explosionEffect, transform.position, transform.rotation);

        Collider[] colliders = Physics.OverlapSphere(transform.position, explosionRadius);

        foreach (Collider nearbyObject in colliders)
        {
            Rigidbody rb = nearbyObject.GetComponent<Rigidbody>();
            if (rb != null)
            {
                rb.AddExplosionForce(explosionForce, transform.position, explosionRadius);
            }

        }
        Destroy(explosionGO, 1f);
        Destroy(gameObject);
    }

    private void Attack()
    {

        enemy.SetDestination(transform.position);

        transform.LookAt(target.transform);


        if(!alreadyAttacked)
        {

            // attack
            float x = UnityEngine.Random.Range(-0.01f, 0.01f);
            float y = UnityEngine.Random.Range(-0.01f, 0.01f);

            Vector3 direction = firePoint.transform.forward + new Vector3(x, y, 0);

            RaycastHit rayHit;
            if (Physics.Raycast(firePoint.transform.position, direction, out rayHit, attackRange))
            {
                Debug.DrawRay(firePoint.position, direction, Color.red);

                GameObject bulletHoleGO = Instantiate(hitEffect, rayHit.point, Quaternion.LookRotation(rayHit.normal));
                Destroy(bulletHoleGO, .1f);
                GameObject shotEffect = Instantiate(hitEffect, firePoint.position, Quaternion.LookRotation(rayHit.normal));
                Destroy(shotEffect, .1f);

                if (rayHit.collider.CompareTag("Player"))
                    rayHit.collider.GetComponentInChildren<PlayerStats>().TakeDamage(20);

                if (rayHit.rigidbody != null)
                {
                    rayHit.rigidbody.AddForce(-rayHit.normal * 30);
                }
            }

            alreadyAttacked = true;
            enemy.SetDestination(target.position); //<=

            Invoke("ResetAttack", timeBetweenAttacks);
        }
    }
    private void ResetAttack()
    {
        alreadyAttacked = false;
    }
    private void Patroling()
    {
        enemy.destination = wayPointList[randomWayPoint].position;
        enemy.Resume();
        if(enemy.remainingDistance <= enemy.stoppingDistance && !enemy.pathPending)
        {
            randomWayPoint = (randomWayPoint + 1) % wayPointList.Count;
        }
    }

    private void ChasePlayer()
    {
        enemy.SetDestination(target.position);
    }
    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, attackRange);
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, losRadius);
    }
    void CheckLOS()
    {
        Vector3 direction = target.position - transform.position;

        float angle = Vector3.Angle(direction, transform.forward);

        if(angle < fieldOfViewAngle * 0.5f)
        {
            RaycastHit hit;
            if(Physics.Raycast(transform.position, direction.normalized, out hit, losRadius))
            {
                if(hit.collider.tag == "Player" || hit.collider.tag == "Shootable")
                {
                    playerIsInLOS = true;
                    Debug.Log("player i LOS");
                }
                else
                {
                    playerIsInLOS = false;
                }
            }
        }
    }
}
