using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    [SerializeField] private static AudioClip hitSound, hurhSound, dieSound, shotSound, reloadSound, playerHurthSound, playerDieSound;
    [SerializeField] private static AudioSource audioSource;

    // Start is called before the first frame update
    void Start()
    {
        playerHurthSound = Resources.Load<AudioClip>("player_hurth");
        hitSound = Resources.Load<AudioClip>("zombie");
        hurhSound = Resources.Load<AudioClip>("zombie_hurth");
        dieSound = Resources.Load<AudioClip>("zombie_die");
        shotSound = Resources.Load<AudioClip>("pistol_shot");
        reloadSound = Resources.Load<AudioClip>("reloading");
        playerDieSound = Resources.Load<AudioClip>("player_die");
        audioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public static void PlaySound(string clip)
    {
        switch (clip)
        {
            case "fire":
                audioSource.PlayOneShot(shotSound);
                break;
            case "reload":
                audioSource.PlayOneShot(reloadSound);
                break;
            case "playerHurt":
                audioSource.PlayOneShot(playerHurthSound);
                break;
            case "playerDie":
                audioSource.PlayOneShot(playerDieSound);
                break;
            case "enemyHit":
                audioSource.PlayOneShot(hitSound);
                break;
            case "enemyDie":
                audioSource.PlayOneShot(dieSound);
                break;
            case "enemyHurt":
                audioSource.PlayOneShot(hurhSound);
                break;


        }
    }
}
