using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dangerous : MonoBehaviour
{
    [SerializeField] private int damage = 10;
    public GameObject entryEffect;
    public void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            Instantiate(entryEffect, other.transform.position, transform.rotation);
            PlayerStats pStats = other.GetComponent<PlayerStats>();
            pStats.Die();

        }
    }
}
