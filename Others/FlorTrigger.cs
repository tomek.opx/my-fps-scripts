using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlorTrigger : MonoBehaviour
{
    public GameObject enemys;
    void OnTriggerStay(Collider other)
    {
        if (other.tag.Equals("Player"))
        {
                enemys.SetActive(true);
            Destroy(gameObject);
            }
        }
    }
