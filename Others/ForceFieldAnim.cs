using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ForceFieldAnim : MonoBehaviour
{
    [SerializeField] private float scroolSpeed = 0.5f, offset;
    public bool U = false, V = true;
    public Material mat;
    // Start is called before the first frame update
    void Start()
    {
        mat = GetComponent<Renderer>().material;
    }

    // Update is called once per frame
    void Update()
    {
        offset = Time.time * scroolSpeed % 1;
        if(U & V)
        {
            mat.mainTextureOffset = new Vector2(offset, offset);
        }else if (U)
        {
            mat.mainTextureOffset = new Vector2(0, offset);
        }
        else if (V)
        {
            mat.mainTextureOffset = new Vector2(offset,0);
        }
    }
}
