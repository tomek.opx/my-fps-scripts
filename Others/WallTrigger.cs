using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallTrigger : MonoBehaviour
{
    public GameObject door, enemys;


    void OnTriggerEnter(Collider other)
    {
        if (other.tag.Equals("Player"))
        {
                door.SetActive(false);
                enemys.SetActive(true);
            Destroy(gameObject);
            }
        }
    }


