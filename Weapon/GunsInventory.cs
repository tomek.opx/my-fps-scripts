using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunsInventory : MonoBehaviour
{
    public GameObject[] gunsList = new GameObject[10];

    public Animator animator;

    private bool[] guns = new bool[] { false, true, false, false, false, false, false, false, false, false };
    private KeyCode[] keys = new KeyCode[] { KeyCode.Alpha0, KeyCode.Alpha1, KeyCode.Alpha2, KeyCode.Alpha3, KeyCode.Alpha4, KeyCode.Alpha5, KeyCode.Alpha6, KeyCode.Alpha7, KeyCode.Alpha8, KeyCode.Alpha9 };
    private int maxGuns = 1, selectedGunID;

    public void addGun(int number)
    {
        guns[number] = true;
        maxGuns++;
    }

    void Update()
    {
        for (int i = 0; i < keys.Length; i++)
        {
            if (Input.GetKeyDown(keys[i]) && guns[i])
            {
                StartCoroutine("WeaponChange");
                selectedGunID = i;
                    //HideGuns();
                   // gunsList[i].SetActive(true);
            }
        }

    }

    private void HideGuns()
    {
        for (int i = 1; i < maxGuns + 1; i++)
        {
            gunsList[i].SetActive(false);
        }
    }
    IEnumerator WeaponChange()
    {
        animator.SetBool("WeaponChange", true);
        yield return new WaitForSeconds(1f);
        animator.SetBool("WeaponChange", false);
        WeaponSwitch();
    }
    private void WeaponSwitch()
    {
        HideGuns();
        gunsList[selectedGunID].SetActive(true);

    }
}

