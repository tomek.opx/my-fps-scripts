using UnityEngine;
using System.Collections;
using TMPro;

public class Shotgun : MonoBehaviour
{
    [SerializeField] private float reloadTime, timeBetweenShooting, timeBetweenShots, spread, range, camShakeMagnitude, camShakeDuration, fireRate = 15f, impactForce = 30f;

    [SerializeField] private int magazineSize, damage, maxAmmo, currentAmmo, bulletsPerTap;

    private int bulletsLeft, bulletsShot;

    [SerializeField] private Transform firePoint, gunContainer;

    [SerializeField] private GameObject bulletHole, shield;

    [SerializeField] private ParticleSystem muzzleFlash;

    [SerializeField] private Texture2D crosshairTexture;

    [SerializeField] private RaycastHit rayHit;

    public Animator animator;

    public Camera fpsCam;

    public Batery batery;

    bool readyToShoot, reloading;

    private float nextTimeToFire = 0f;

    private Rect position;

    public TextMeshProUGUI text;

    private float zoomFieldOfView = 40.0f;
    private float defaultFieldOfView = 60.0f;

    /// recoil
    /// 
    public WeaponRecoil weaponRecoil;


    private void Awake()
    {
        bulletsLeft = magazineSize;
        currentAmmo = maxAmmo;
        readyToShoot = true;

        Cursor.visible = false;
        position = new Rect((Screen.width - crosshairTexture.width) / 2,
                     (Screen.height - crosshairTexture.height) / 2,
                     crosshairTexture.width,
                     crosshairTexture.height);

    }
    void OnGUI()
    {
        GUI.DrawTexture(position, crosshairTexture);
    }


    void Update()
    {
        text.SetText(bulletsLeft + " / " + currentAmmo);

        MyInput();

    }

    private void MyInput()
    {/*
        Aim(Input.GetButton("Fire2"));
        if (Input.GetKeyDown(KeyCode.R) && bulletsLeft < magazineSize && !reloading) StartCoroutine("Reload");

        if (Input.GetButton("Fire1") && Time.time >= nextTimeToFire && bulletsLeft > 0)
        {
            nextTimeToFire = Time.time + 1f / fireRate;
            Shoot();
        }*/
        UseShield(Input.GetKey(KeyCode.Q));
        if (readyToShoot)
        {
            Aim(Input.GetButton("Fire2"));
            if (Input.GetKeyDown(KeyCode.R) && bulletsLeft < magazineSize && !reloading) StartCoroutine("Reload");

            if (Input.GetButton("Fire1") && Time.time >= nextTimeToFire && bulletsLeft > 0)
            {
                nextTimeToFire = Time.time + 1f / fireRate;
                Shoot();
            }
            if (bulletsLeft <= 0)
            {
                StartCoroutine("Reload");
            }

        }

    }

    private void Aim(bool isAiming)
    {
        if (isAiming)
        {
            if (fpsCam.fieldOfView > zoomFieldOfView)
            {
                fpsCam.fieldOfView--;
                animator.SetBool("Aiming", true);

            }

        }
        else
        {
            if (fpsCam.fieldOfView < defaultFieldOfView)
            {
                fpsCam.fieldOfView++;
                animator.SetBool("Aiming", false);

            }

        }

    }
    private void Shoot()
    {
        readyToShoot = false;

        weaponRecoil.Fire();



        float x = Random.Range(-spread, spread);
        float y = Random.Range(-spread, spread);

        Vector3 direction = fpsCam.transform.forward + new Vector3(x, y, 0);

        if (Physics.Raycast(fpsCam.transform.position, direction, out rayHit, range)) 
        {
            Debug.Log(rayHit.collider.name);

            Debug.DrawRay(firePoint.position, direction, Color.green);

            if (rayHit.collider.CompareTag("Enemy"))
                rayHit.collider.GetComponentInChildren<EnemyAI>().TakeDamage(damage);
            if (rayHit.collider.CompareTag("Boss"))
                rayHit.collider.GetComponentInParent<Enemy2>().TakeDamage(damage);

            GameObject bulletHoleGO = Instantiate(bulletHole, rayHit.point, Quaternion.LookRotation(rayHit.normal));
            Destroy(bulletHoleGO, 0.1f);


            if (rayHit.rigidbody != null)
            {
                rayHit.rigidbody.AddForce(-rayHit.normal * impactForce);
            }
        }
        if (Physics.Raycast(fpsCam.transform.position, direction + new Vector3(Random.Range(-spread, spread), Random.Range(-spread, spread), 0f), out rayHit, range)) 
        {
            Debug.Log(rayHit.collider.name);

            Debug.DrawRay(firePoint.position, direction, Color.green);

            if (rayHit.collider.CompareTag("Enemy"))
                rayHit.collider.GetComponentInChildren<EnemyAI>().TakeDamage(damage);
            if (rayHit.collider.CompareTag("Boss"))
                rayHit.collider.GetComponentInParent<Enemy2>().TakeDamage(damage);

            GameObject bulletHoleGO = Instantiate(bulletHole, rayHit.point, Quaternion.LookRotation(rayHit.normal));
            Destroy(bulletHoleGO, 0.1f);


            if (rayHit.rigidbody != null)
            {
                rayHit.rigidbody.AddForce(-rayHit.normal * impactForce);
            }
        }
        if (Physics.Raycast(fpsCam.transform.position, direction + new Vector3(Random.Range(-spread, spread), Random.Range(-spread, spread), 0f ), out rayHit, range)) 
        {
            Debug.Log(rayHit.collider.name);

            Debug.DrawRay(firePoint.position, direction, Color.green);

            if (rayHit.collider.CompareTag("Enemy"))
                rayHit.collider.GetComponentInChildren<EnemyAI>().TakeDamage(damage);
            if (rayHit.collider.CompareTag("Boss"))
                rayHit.collider.GetComponentInParent<Enemy2>().TakeDamage(damage);

            GameObject bulletHoleGO = Instantiate(bulletHole, rayHit.point, Quaternion.LookRotation(rayHit.normal));
            Destroy(bulletHoleGO, 0.1f);


            if (rayHit.rigidbody != null)
            {
                rayHit.rigidbody.AddForce(-rayHit.normal * impactForce);
            }
        }
        if (Physics.Raycast(fpsCam.transform.position, direction + new Vector3(Random.Range(-spread, spread), Random.Range(-spread, spread), 0f), out rayHit, range)) 
        {
            Debug.Log(rayHit.collider.name);

            Debug.DrawRay(firePoint.position, direction, Color.green);

            if (rayHit.collider.CompareTag("Enemy"))
                rayHit.collider.GetComponentInChildren<EnemyAI>().TakeDamage(damage);
            if (rayHit.collider.CompareTag("Boss"))
                rayHit.collider.GetComponentInParent<Enemy2>().TakeDamage(damage);

            GameObject bulletHoleGO = Instantiate(bulletHole, rayHit.point, Quaternion.LookRotation(rayHit.normal));
            Destroy(bulletHoleGO, 0.1f);


            if (rayHit.rigidbody != null)
            {
                rayHit.rigidbody.AddForce(-rayHit.normal * impactForce);
            }
        }
        if (Physics.Raycast(fpsCam.transform.position, direction + new Vector3(Random.Range(-spread, spread), Random.Range(-spread, spread), 0f), out rayHit, range)) 
        {
            Debug.Log(rayHit.collider.name);

            Debug.DrawRay(firePoint.position, direction, Color.green);

            if (rayHit.collider.CompareTag("Enemy"))
                rayHit.collider.GetComponentInChildren<EnemyAI>().TakeDamage(damage);
            if (rayHit.collider.CompareTag("Boss"))
                rayHit.collider.GetComponentInParent<Enemy2>().TakeDamage(damage);

            GameObject bulletHoleGO = Instantiate(bulletHole, rayHit.point, Quaternion.LookRotation(rayHit.normal));
            Destroy(bulletHoleGO, 0.1f);


            if (rayHit.rigidbody != null)
            {
                rayHit.rigidbody.AddForce(-rayHit.normal * impactForce);
            }
        }
        //SoundManager.PlaySound("fire");

        muzzleFlash.Play();

        bulletsLeft--;
        bulletsShot--;

        Invoke("ResetShot", timeBetweenShooting);

        if (bulletsShot > 0 && bulletsLeft > 0)
            Invoke("Shoot", timeBetweenShots);
    }
    private void ResetShot()
    {
        readyToShoot = true;
    }

    IEnumerator Reload()
    {
        reloading = true;
        animator.SetBool("Reloading", true);

        yield return new WaitForSeconds(reloadTime);
        ReloadingFinished();

        //SoundManager.PlaySound("reload");

        animator.SetBool("Reloading", false);
    }
    private void ReloadingFinished()
    {
        if (bulletsLeft > 0)
        {
            currentAmmo += bulletsLeft;
        }
        if (currentAmmo > magazineSize)
        {
            bulletsLeft = magazineSize;
            currentAmmo -= magazineSize;

        }
        else if (currentAmmo < magazineSize)
        {

            bulletsLeft = currentAmmo;
            currentAmmo = 0;
        }

        reloading = false;
    }

    private void UseShield(bool useShield)
    {
        if (useShield && batery.CurrentBatery > 0)
        {
            shield.SetActive(true);
            readyToShoot = false;
            batery.UseBatery(0.1f);

        }
        else if(batery.CurrentBatery <= 0)
        {
            shield.SetActive(false);
            readyToShoot = true;

        }
    }

    ////////////////////////////////////
    public bool canGetAmmo()
    {
        if (currentAmmo == maxAmmo)
        {
            return false;
        }
        return true;
    }

    public void AddAmmo()
    {
        currentAmmo = maxAmmo;
    }
}
