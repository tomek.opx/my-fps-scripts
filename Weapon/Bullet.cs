using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    [SerializeField] private int dmg = 20;
    [SerializeField] private GameObject hitEffect;

    public void OnTriggerEnter(Collider other)
    {
        if (other.tag != "Player")
        {
            Debug.Log("hit " + other.tag);
            GameObject bulletHoleGO = Instantiate(hitEffect, other.transform.position, Quaternion.identity);
            Destroy(bulletHoleGO, .1f);
            Destroy(gameObject,.1f);

        }

        if (other.tag == "Player")
        {
            PlayerStats pStats = other.GetComponent<PlayerStats>();
            GameObject bulletHoleGO = Instantiate(hitEffect, other.transform.position, Quaternion.identity);
            Destroy(bulletHoleGO, .1f);

            pStats.TakeDamage(dmg);
            Destroy(gameObject,.1f);

        }
    }
}
