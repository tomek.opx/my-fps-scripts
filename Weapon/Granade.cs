using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Granade : MonoBehaviour
{

    [SerializeField] private float delay = 3f, radius =5f, force = 700;
    [SerializeField] private GameObject explosionEffect;
    [SerializeField] private int explosionDamage = 100;

    float countdown;
    bool hasExploded = false;

    // Start is called before the first frame update
    void Start()
    {
        countdown = delay;
    }

    // Update is called once per frame
    void Update()
    {
        countdown -= Time.deltaTime;
        if(countdown <= 0 && !hasExploded)
        {
            Explode();
            hasExploded = true;
        }
    }

    private void Explode()
    {
        GameObject explosionGO = Instantiate(explosionEffect, transform.position, transform.rotation);

        Collider[] colliders = Physics.OverlapSphere(transform.position, radius);

        foreach(Collider nearbyObject in colliders)
        {
            Rigidbody rb = nearbyObject.GetComponent<Rigidbody>();
            if(rb != null)
            {
                rb.AddExplosionForce(force, transform.position, radius);
            }
            if (nearbyObject.tag == "Boss")
            {
                Enemy2 pStats = nearbyObject.GetComponent<Enemy2>();
                if (pStats != null)
                {
                    pStats.TakeDamage(explosionDamage);
                }
            }
            if (nearbyObject.tag == "Enemy")
            {
                EnemyAI enemy = nearbyObject.GetComponent<EnemyAI>();
                if (enemy != null)
                {
                    enemy.TakeDamage(explosionDamage);
                }
            }
            if (nearbyObject.tag == "Player")
            {
                PlayerStats enemy = nearbyObject.GetComponent<PlayerStats>();
                if (enemy != null)
                {
                    enemy.TakeDamage(explosionDamage);
                }
            }

        }
        Destroy(explosionGO, 1f);
        Destroy(gameObject);
    }
}
