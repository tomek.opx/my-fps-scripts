using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class GranadeThrower : MonoBehaviour
{
    public float throwForce = 700f;
    public GameObject granadePrefab;
    [SerializeField] private int granadeNumber;
    public TextMeshProUGUI text;


    // Update is called once per frame
    void Update()
    {
        text.SetText("G: " + granadeNumber);

        if (Input.GetKeyDown(KeyCode.G) && granadeNumber > 0)
        {
            ThrowGranade();
        }
    }

    private void ThrowGranade()
    {
        GameObject granade = Instantiate(granadePrefab, transform.position, transform.rotation);
        Rigidbody rb = granade.GetComponent<Rigidbody>();
        rb.AddForce(transform.forward * throwForce);
        granadeNumber--;
    }
    public void AddGranade(int add)
    {
        granadeNumber += add;
    }
}
