using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponPickup : MonoBehaviour
{
    public int weaponNumber;

    private bool isPlayer;

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag.Equals("Player"))
        {
            other.SendMessage("addGun", weaponNumber);


            Destroy(gameObject);

        }

    }
}