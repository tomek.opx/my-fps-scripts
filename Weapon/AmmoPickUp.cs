using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AmmoPickUp : MonoBehaviour
{
    public bool addHP, addAmmo, multiuse;
    private bool isPlayer;
    [SerializeField] private int addGranade;

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag.Equals("Player"))
        {
            if (addHP)
                other.SendMessage("RestoreHP");

            other.GetComponentInChildren<GranadeThrower>().AddGranade(addGranade);

            if (addAmmo)
            {
                if (other.GetComponentInChildren<Gun>() != null)
                {
                    other.GetComponentInChildren<Gun>().AddAmmo();
                    if (!multiuse)
                        Destroy(gameObject);

                }
                else if (other.GetComponentInChildren<Shotgun>() != null)
                {
                    other.GetComponentInChildren<Shotgun>().AddAmmo();
                    if (!multiuse)
                        Destroy(gameObject);

                }

            }
            if (!multiuse)
                Destroy(gameObject);

        }
    }
   /* void OnTriggerStay(Collider other)
    {
        if (other.tag.Equals("Player"))
        {
            isPlayer = true;
            if (Input.GetKeyDown(KeyCode.E))
            {
                if(addHP)
                other.SendMessage("RestoreHP");

                other.GetComponentInChildren<GranadeThrower>().AddGranade(addGranade);

                if (addAmmo)
                {
                    if (other.GetComponentInChildren<Gun>() != null)
                    {
                        other.GetComponentInChildren<Gun>().AddAmmo();
                        if(!multiuse)
                        Destroy(gameObject);

                    }
                    else if (other.GetComponentInChildren<Shotgun>() != null)
                    {
                        other.GetComponentInChildren<Shotgun>().AddAmmo();
                        if (!multiuse)
                            Destroy(gameObject);

                    }

                }
                if (!multiuse)
                    Destroy(gameObject);

            }

        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.tag.Equals("Player"))
        {
            isPlayer = false;
        }
    }

    void OnGUI()
    {
        if (isPlayer)
        {
            GUI.Label(new Rect(Screen.width / 2 - 50, Screen.height / 2 - 10, 100, 20), "Press E to grab");
        }
    }*/
}